package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Uncommon {
    // Stat modifier
    private double powerModifier = 1.2;
    // Color for display purposes
    private String itemRarityColor = Color.GREEN;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
