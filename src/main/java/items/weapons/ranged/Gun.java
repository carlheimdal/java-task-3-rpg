package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Gun extends Weapon {
    public Gun() {
        super();
        this.attackPowerModifier = WeaponStatsModifiers.GUN_ATTACK_MOD;
    }

    public Gun(double itemRarityModifer) {
        super(itemRarityModifer);
        this.attackPowerModifier = WeaponStatsModifiers.GUN_ATTACK_MOD;
    }
}
