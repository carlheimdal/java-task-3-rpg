package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Crossbow extends Weapon {
    public Crossbow() {
        super();
        this.attackPowerModifier = WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    }

    public Crossbow(double itemRarityModifer) {
        super(itemRarityModifer);
        this.attackPowerModifier = WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    }
}
