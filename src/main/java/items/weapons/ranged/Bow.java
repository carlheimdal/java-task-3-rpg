package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Bow extends Weapon {

    public Bow() {
        super();
        this.attackPowerModifier = WeaponStatsModifiers.BOW_ATTACK_MOD;
    }

    public Bow(double itemRarityModifer) {
        super(itemRarityModifer);
        this.attackPowerModifier = WeaponStatsModifiers.BOW_ATTACK_MOD;
    }
}
