package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.MeleeWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Sword extends MeleeWeapon {

    public Sword(){
        super();
        this.attackPowerModifier = WeaponStatsModifiers.SWORD_ATTACK_MOD;
    }

    public Sword (double itemRarityModifer) {
        super(itemRarityModifer);
        this.attackPowerModifier = WeaponStatsModifiers.SWORD_ATTACK_MOD;
    }
}
