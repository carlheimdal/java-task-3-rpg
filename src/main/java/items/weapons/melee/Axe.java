package main.java.items.weapons.melee;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.MeleeWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Axe extends MeleeWeapon {

    public Axe(){
        super();
        this.attackPowerModifier = WeaponStatsModifiers.AXE_ATTACK_MOD;
    }

    public Axe(double itemRarityModifer) {
        super(itemRarityModifer);
        this.attackPowerModifier = WeaponStatsModifiers.AXE_ATTACK_MOD;
    }
}