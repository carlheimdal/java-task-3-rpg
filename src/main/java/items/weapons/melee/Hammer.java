package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.MeleeWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Hammer extends MeleeWeapon {

    public Hammer(){
        this.attackPowerModifier = WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    }

    public Hammer(double itemRarityModifer) {
        super(itemRarityModifer);
        this.attackPowerModifier = WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    }
}
