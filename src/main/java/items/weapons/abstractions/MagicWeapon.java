package main.java.items.weapons.abstractions;

public class MagicWeapon extends Weapon {

    public MagicWeapon(){
        super();
    }

    public MagicWeapon(double itemRarityModifier){
        super(itemRarityModifier);
    }
}
