package main.java.items.weapons.abstractions;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public abstract class Weapon {

    // Weapon stats
    protected double itemRarityModifier;
    protected double attackPowerModifier;

    // Public props
    public double getRarity() {
        return itemRarityModifier;
    }
    public double getattackPowerModifier(){
        return attackPowerModifier;
    }

    // Ctors

    // Without rarity (rarity = common)
    public Weapon(){
        this.itemRarityModifier = ItemRarityModifiers.COMMON_RARITY_MODIFIER;
    }
    // With rarity
    public Weapon(double itemRarityModifier){
        this.itemRarityModifier = itemRarityModifier;
    }
}