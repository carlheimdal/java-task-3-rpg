package main.java.items.weapons.abstractions;

import main.java.characters.abstractions.Melee;

public class MeleeWeapon extends Weapon {

    // No rarity provided
    public MeleeWeapon(){
        super();
    }
    // Rarity provided
    public MeleeWeapon(double itemRarityModifier){
        super(itemRarityModifier);
    }
}
