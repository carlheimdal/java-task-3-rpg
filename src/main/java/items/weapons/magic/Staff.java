package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Staff extends MagicWeapon {

    public Staff(){
        super();
        this.attackPowerModifier = WeaponStatsModifiers.STAFF_MAGIC_MOD;
    }

    public Staff(double itemRarityModifer) {
        super(itemRarityModifer);
        this.attackPowerModifier = WeaponStatsModifiers.STAFF_MAGIC_MOD;
    }
}
