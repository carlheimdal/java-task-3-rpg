package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Wand extends MagicWeapon {

    public Wand(){
        super();
        this.attackPowerModifier = WeaponStatsModifiers.WAND_MAGIC_MOD;
    }

    public Wand(double itemRarityModifer) {
        super(itemRarityModifer);
        this.attackPowerModifier = WeaponStatsModifiers.WAND_MAGIC_MOD;
    }
}
