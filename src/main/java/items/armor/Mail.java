package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.ItemRarity;

public class Mail extends Armor {

    public Mail(double itemRarity) {
        super(itemRarity,
                ArmorStatsModifiers.MAIL_HEALTH_MODIFIER,
                ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER
        );
    }
}
