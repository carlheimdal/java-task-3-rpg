package main.java.items.armor.abstractions;

public class Armor {

    // Modifiers
    protected double rarityModifier, healthModifier, physRedModifier, magicRedModifier;

    protected Armor(double rarityModifier,
          double healthModifer,
          double physRedModifier,
          double magicRedModifier){
        this.rarityModifier = rarityModifier;
        this.healthModifier = healthModifer;
        this.physRedModifier = physRedModifier;
        this.magicRedModifier = magicRedModifier;
    }

    // Public properties
    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    public double getRarityModifier() {
        return rarityModifier;
    }
}
