package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

public class Leather extends Armor {

    public Leather(double itemRarity) {
        super(itemRarity,
                ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER,
                ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER
        );
    }
}
