package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Support;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.magic.Staff;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends Support {

    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    private ShieldingSpell shieldingSpell;

    // Constructor
    public Priest(Cloth cloth, Staff staff, ShieldingSpell shieldingSpell) {
        super(CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH,
                CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES);
        this.armor = cloth;
        this.weapon = staff;
        this.shieldingSpell = shieldingSpell;
    }

    // Character behaviours

    //Shields a party member for a percentage of their maximum health.
    public double shieldPartyMember(double partyMemberMaxHealth) {
        return 0; // Return calculated shield value
    }

}
