package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.Support;
import main.java.factories.SpellFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Leather;
import main.java.items.weapons.magic.Wand;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.SpellCategory;
import main.java.spells.abstractions.SpellType;
import main.java.spells.healing.Regrowth;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends Support {

    // Spell
    private HealingSpell healingSpell;

    // Constructor
    public Druid(Leather leather, Wand wand, HealingSpell healingSpell) {
        super(CharacterBaseStatsDefensive.DRUID_BASE_HEALTH,
                CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES);
        this.armor = leather;
        this.weapon = wand;
        this.healingSpell = healingSpell;
    }

    // Heals the party member, increasing their current health.
    public void healPartyMember(Character partyMember) {
        partyMember.updateCurrentHealth(this.healingSpell.getHealingAmount());
    }
}