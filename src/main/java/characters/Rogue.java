package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Melee;
import main.java.characters.abstractions.Ranged;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends Melee {

    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BladeWeapon;

    // Constructor
    public Rogue(Armor leather, Weapon axe) {
        super(CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH,
                CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES);
        this.armor = leather;
        this.weapon = axe;
        this.baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;
    }

    // Damages the enemy
    public double attackWithBladedWeapon() {
        return 0; // Replaced with actual damage amount based on calculations
    }
}
