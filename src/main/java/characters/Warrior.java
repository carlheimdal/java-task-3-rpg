package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends Melee {

    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Plate;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BladeWeapon;

    // Constructor
    public Warrior(Plate plate, Weapon weapon) {
        super(CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES);
        this.armor = plate;
        this.weapon = weapon;
        this.baseAttackPower = CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER;
    }

    // Damages the enemy
    public double attackWithBladedWeapon() {
        return 0; // Replaced with actual damage amount based on calculations
    }
}
