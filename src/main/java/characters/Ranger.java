package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Ranged;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Mail;
import main.java.items.weapons.ranged.Bow;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends Ranged {

    private double attackPower;

    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Mail;

    public Ranger(Mail mail, Bow bow){
        super(CharacterBaseStatsDefensive.RANGER_BASE_HEALTH,
                CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES);
        this.armor = mail;
        this.weapon = bow;
        this.attackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
    }

    // Equipment behaviours

    // Equips armor to the character, modifying stats.
    public void equipArmor(Mail armor) {
        this.armor = armor;
    }

    // Character behaviours

    // Damages the enemy
    public double attackWithRangedWeapon() {
        return 0; // Replaced with actual damage amount based on calculations
    }
}
