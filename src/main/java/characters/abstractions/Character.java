package main.java.characters.abstractions;

import main.java.factories.ArmorFactory;
import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public abstract class Character {

    // Active trackers and flags
    protected double currentHealth;
    protected Boolean isDead = false;
    protected double baseHealth, basePhysReductionPercent, baseMagicReductionPercent;
    // Armor
    protected Armor armor;
    // Weapon
    protected Weapon weapon;

    // Ctor
    Character(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent){
        this.currentHealth = baseHealth;
        this.baseHealth = baseHealth;
        this.basePhysReductionPercent = basePhysReductionPercent;
        this.baseMagicReductionPercent = baseMagicReductionPercent;
    }

    // Public getters statuses and stats
    public Boolean getDead(){
        return this.isDead;
    }

    public double getCurrentHealth(){
        return this.currentHealth;
    }

    public double getCurrentMaxHealth() {
        return baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
    }

    // Set health
    public void updateCurrentHealth(double changeHealth){this.currentHealth *= changeHealth;}

    // Equip Armor
    public void equipArmor(Armor armor){
        this.armor = armor;
    }

    // Equip Weapon
    public void equipWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    // Take Damage
    public double takeDamage(double incomingDamage, String damageType) {
        return 0; // Return damage taken after damage reductions, based on type of damage.
    }
}