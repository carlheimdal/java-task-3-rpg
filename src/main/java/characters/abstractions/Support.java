package main.java.characters.abstractions;

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class Support extends Character {
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;

    // Ctor
    protected Support(double baseHealth, double basePhysRed, double baseMagicRed){
        super(baseHealth, basePhysRed, baseMagicRed);
    }
}
