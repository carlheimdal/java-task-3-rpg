package main.java.characters.abstractions;

import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class Ranged extends Character {
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Ranged;

    // Ctor
    public Ranged(double baseHealth,
           double basePhysRedcutionPercent,
           double baseMagicReductionPercent){
        super(baseHealth,
                basePhysRedcutionPercent,
                baseMagicReductionPercent);
    }
}
