package main.java.characters.abstractions;

import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;

public abstract class Caster extends Character {

    // Ctor
    public Caster(double baseHealth, double basePhysRed, double baseMagicRes){
        super(baseHealth, basePhysRed, baseMagicRes);
    }

    // Armor
    protected Cloth armor;

    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    protected DamagingSpell damagingSpell;

    //Equips armor to the character, modifying stats.
    public void equipArmor(Cloth armor) {

    }

    // Damages the enemy with its spells
    public double castDamagingSpell() {
        return 0; // Replaced with actual damage amount based on calculations
    }
}
