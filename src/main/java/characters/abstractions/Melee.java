package main.java.characters.abstractions;

public abstract class Melee extends Character {

    // stats
    protected double baseAttackPower;

    // Ctor
    public Melee(double baseHealth,
                  double basePhysRedcutionPercent,
                  double baseMagicReductionPercent){
        super(baseHealth,
                basePhysRedcutionPercent,
                baseMagicReductionPercent);
    }
}
