package main.java.characters.abstractions;
/*
 This enumerator exists to serve as a placeholder for a character category.
 This should be replaced by some class hierarchy with these categories as parent classes
 to the Character classes. There should be some base Character class as well, that these Category subclasses
 inherit from.
*/
public enum CharacterCategory {
    Caster,
    Ranged,
    Melee,
    Support
}