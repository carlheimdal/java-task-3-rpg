package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.magic.Staff;
import main.java.spells.abstractions.DamagingSpell;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends Caster {

    // Base stats offensive
    private double baseMagicPower;

    // Ctor
    public Mage(Cloth cloth, Staff staff, DamagingSpell damagingSpell) {
        super(CharacterBaseStatsDefensive.MAGE_BASE_HEALTH,
                CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES);
        this.baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;
        this.armor = cloth;
        this.weapon = staff;
        this.damagingSpell = damagingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
    }
}
