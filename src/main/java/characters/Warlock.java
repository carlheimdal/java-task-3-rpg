package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;

/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends Caster {

    private double baseMagicPower;

    // Ctors
    public Warlock(Cloth cloth, Weapon weapon, DamagingSpell damagingSpell) {
        super(CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES);
        this.armor = cloth;
        this.weapon = weapon;
        this.baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;
        this.damagingSpell = damagingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
    }
}
