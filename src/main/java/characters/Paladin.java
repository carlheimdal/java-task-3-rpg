package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.melee.Hammer;

/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends Melee {

    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Plate;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BluntWeapon;

    // Constructor
    public Paladin(Plate plate, Hammer hammer) {
        super(CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH,
                CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES);
        this.baseAttackPower = CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;
        this.armor = plate;
        this.weapon = hammer;
    }

    // Damages the enemy
    public double attackWithBluntWeapon() {
        return baseAttackPower * this.weapon.getattackPowerModifier() * this.weapon.getRarity();
    }
}
