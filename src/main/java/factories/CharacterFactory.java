package main.java.factories;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.basestats.ItemRarityModifiers;
import main.java.characters.*;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.CharacterType;
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.Axe;
import main.java.items.weapons.melee.Dagger;
import main.java.items.weapons.melee.Hammer;
import main.java.items.weapons.ranged.Bow;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.shielding.Barrier;

/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
public class CharacterFactory {
    public Character getCharacter(CharacterType characterType) {
        switch(characterType) {
            case Druid:
                Leather leather = new Leather(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                Wand wand = new Wand(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                HealingSpell healingSpell = new Regrowth();
                return new Druid(leather, wand, healingSpell);
            case Mage:
                Cloth cloth = new Cloth(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                Staff staff = new Staff(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                DamagingSpell damagingSpell = new ArcaneMissile();
                return new Mage(cloth, staff, damagingSpell);
            case Paladin:
                Plate plate = new Plate(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                Hammer hammer = new Hammer(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                return new Paladin(plate, hammer);
            case Priest:
                Cloth priestCloth = new Cloth(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                Staff priestStaff = new Staff(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                ShieldingSpell shieldingSpell = new Barrier();
                return new Priest(priestCloth, priestStaff, shieldingSpell);
            case Ranger:
                Mail mail = new Mail(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                Bow bow = new Bow(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                return new Ranger(mail, bow);
            case Rogue:
                Leather rogueLeather = new Leather(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                Axe axe = new Axe(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                return new Rogue(rogueLeather, axe);
            case Warlock:
                Cloth warlockCloth = new Cloth(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                Wand warlockWand = new Wand(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                DamagingSpell dmg = new ChaosBolt();
                return new Warlock(warlockCloth, warlockWand, dmg);
            case Warrior:
                Plate warriorPlate = new Plate(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                Dagger dagg = new Dagger(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
                return new Warrior(warriorPlate, dagg);
            default:
                return null;
        }
    }
}
