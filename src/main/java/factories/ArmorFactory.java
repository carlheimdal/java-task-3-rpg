package main.java.factories;
// Imports
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

/*
 This factory exists to be responsible for creating new Armor.
 Object is replaced with some Item abstraction.
*/
public class ArmorFactory {
    public Armor getArmor(ArmorType armorType, double rarity){
        switch(armorType) {
            case Cloth:
                return new Cloth(rarity);
            case Leather:
                return new Leather(rarity);
            case Mail:
                return new Mail(rarity);
            case Plate:
                return new Plate(rarity);
            default:
                return null;
        }
    }
}
