package main.java.factories;

import main.java.characters.abstractions.CharacterCategory;
import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.healing.SwiftMend;
import main.java.spells.shielding.Barrier;
import main.java.spells.shielding.Rapture;

// TODO Fill in the blanks using the given spells
public class SpellFactory {
    public Spell getSpell(String spell) {
        return switch (spell) {
            case ("ArcaneMissile") -> new ArcaneMissile();
            case ("ChaosBolt") -> new ChaosBolt();
            case ("Regrowth") -> new Regrowth();
            case ("SwiftMend") -> new SwiftMend();
            case ("Barrier") -> new Barrier();
            case ("Rapture") -> new Rapture();
            default -> null;
        };
    }
}

