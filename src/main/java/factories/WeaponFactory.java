package main.java.factories;
// Imports
import main.java.basestats.ItemRarityModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.*;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;
/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Weapon as a return type when refactored to be good OO design.
*/
public class WeaponFactory {
    // When rarity is provided
    public Weapon getItem(WeaponType weaponType, double rarity) {
        return switch (weaponType) {
            case Axe -> new Axe(rarity);
            case Bow -> new Bow(rarity);
            case Crossbow -> new Crossbow (rarity);
            case Dagger -> new Dagger(rarity);
            case Gun -> new Gun(rarity);
            case Hammer -> new Hammer(rarity);
            case Mace -> new Mace(rarity);
            case Staff -> new Staff(rarity);
            case Sword -> new Sword(rarity);
            case Wand -> new Wand(rarity);
            default -> null;
        };
    }
    // When rarity is not provided
    public Weapon getItem(WeaponType weaponType) {
        return switch (weaponType) {
            case Axe -> new Axe();
            case Bow -> new Bow();
            case Crossbow -> new Crossbow();
            case Dagger -> new Dagger();
            case Gun -> new Gun();
            case Hammer -> new Hammer();
            case Mace -> new Mace();
            case Staff -> new Staff();
            case Sword -> new Sword();
            case Wand -> new Wand();
            default -> null;
        };
    }
}
