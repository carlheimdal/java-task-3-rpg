package main.java.factories;
// Imports
import main.java.enemies.*;
import main.java.enemies.abstractions.Enemy;
import main.java.enemies.abstractions.EnemyType;
/*
 This factory exists to be responsible for creating new enemies.
*/
public class EnemyFactory {
    public Enemy getEnemy(EnemyType enemyType, double difficultyModifier, double floorLevel){
        return switch (enemyType) {
            case Beast -> new Beast(difficultyModifier, floorLevel);
            case Dragon -> new Dragon(difficultyModifier, floorLevel);
            case Elemental -> new Elemental(difficultyModifier, floorLevel);
            case Fiend -> new Fiend(difficultyModifier, floorLevel);
            case Giant -> new Giant(difficultyModifier, floorLevel);
            case Humanoid -> new Humanoid(difficultyModifier, floorLevel);
            case Orc -> new Orc(difficultyModifier, floorLevel);
            case Undead -> new Undead(difficultyModifier, floorLevel);
        };
    }
}
